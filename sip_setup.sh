
#! /bin/bash
###
# script: easy install SIP and requirements on a fresh Bullseye Pi image
# by: Gerard
# version: 0.95
# usage:  curl -sSL https://gitlab.com/seventer/sip_setup/-/raw/main/sip_setup.sh | sudo bash
###


if [[ $(id -u) -gt 0 ]]
then
    echo "run this command as root or sudo otherwise this script might fail"
    exit
fi

current_user=${SUDO_USER:-$USER}

do_upd_sys=false
do_i2c=false
do_mqtt=false
do_user_grp=false
do_log2ram=false
install_location="/opt"

CHOICES=$(whiptail --title " SIP setup " --separate-output --checklist  "Choose install options" 12 45 5 \
 "1" "Update system (recommended)" ON \
 "2" "Enable i2c" ON \
 "3" "Install MQTT broker" OFF \
 "4" "Adjust user permissions" ON \
 "5" "Install log2ram" ON 3>&1 1>&2 2>&3)

if [ -z "$CHOICES" ]; then
  echo "No option was selected or cancelled. Stopping script."
  exit 1
else
  for CHOICE in $CHOICES; do
    case "$CHOICE" in
    "1")
      do_upd_sys=true
      ;;
    "2")
      do_i2c=true
      ;;
    "3")
      do_mqtt=true
      ;;
    "4")
      do_user_grp=true
      ;;
    "5")
      do_log2ram=true
      ;;
    *)
      echo "Unsupported item $CHOICE!" >&2
      exit 1
      ;;
    esac
  done
fi

if (whiptail --title "Location" --yesno "Install SIP in /opt or the $current_user homedir?" --no-button "homedir" --yes-button "opt" 8 45); then
    echo "installing in /opt"
    mkdir -p /opt
    install_location="/opt"
else
    echo "installing in homedir"
    install_location="/home/$current_user"
fi


if [ "$do_upd_sys" = true ]; then
  echo ===== Updating system =====
  sudo apt update && sudo apt upgrade -y
fi

echo ===== Installing git =====
sudo apt install git -y

if [ "$do_i2c" = true ]; then
  echo ===== Installing  i2c requirements =====
  sudo apt install -y python3-smbus i2c-tools -y
  echo ===== Enabling  i2c interface =====
  sudo raspi-config nonint do_i2c 0
fi

if [ "$do_mqtt" = true ]; then
  echo ===== Installing  mqtt broker and paho client =====
  sudo apt install mosquitto -y
  sudo apt install python3-paho-mqtt -y
fi

if [ "$do_user_grp" = true ]; then
  echo ===== adding user ${current_user} to hardware groups  =====
  sudo usermod -aG gpio ${current_user}
  sudo usermod -aG i2c ${current_user}
  sudo usermod -aG dialout ${current_user}
fi

echo ===== Installing SIP =====
cd $install_location
git clone https://github.com/Dan-in-CA/SIP


echo ===== Creating and installing SystemD service =====
cat << EOF >> /tmp/sip.service
#Service for SIP running on a SystemD service
#
[Unit]
Description=SIP for Python3
After=syslog.target network.target
[Service]
ExecStart=/usr/bin/python3 -u ${install_location}/SIP/sip.py
Restart=on-abort
WorkingDirectory=${install_location}/SIP/
SyslogIdentifier=sip
[Install]
WantedBy=multi-user.target
EOF

sudo cp /tmp/sip.service /etc/systemd/system/
sudo systemctl enable sip.service


if [ "$do_log2ram" = true ]; then
  echo ===== Installing log2ram =====
  cd /home/${current_user}
  wget https://github.com/azlux/log2ram/archive/master.tar.gz -O log2ram.tar.gz
  tar xf log2ram.tar.gz
  cd /home/${current_user}/log2ram-master
  sudo ./install.sh
  echo ===== Increasing logsize to 100M =====
  sed -i "s/40M/100M/g" /etc/log2ram.conf
  cd ~
fi

echo ===== Done installing SIP and requirements. Please check the output above and reboot the Pi =====

if (whiptail --title "Setup finished" --yesno "Done installing SIP and requirements.\n\nReboot now or choose Exit to review the output of the setup " --no-button "Exit" --yes-button "Reboot" 10 65); then
    echo "Restarting....."
    sudo reboot
else
    exit 1
fi
