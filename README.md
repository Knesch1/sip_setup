# sip_setup

Easy setup of [SIP](https://dan-in-ca.github.io/SIP/) (**S**ustainable **I**rrigation **P**latform) on a fresh Bulleye Raspberry Pi image.

This setup optionally
- updates the Pi
    - takes time but recommended
- enables i2c
    - for use with i2c devices like pcf8574 or oled display 
- installs a MQTT broker
    - this installs the Mosquitto broker and paho library
- install log2ram
    - reduce writes to SD card by moving /var/log to ram
- Adjust user permissions
    - allow SIP to run as a regual user. Although supported, there are additional requirements to setup (ask author if you want to know howto)
- installs SIP in /opt (recommended) or user directory
    - to install SIP as a service it is recommended to install it in the /opt directory. Alternatively the old location (user home dir) can be selected




## Getting started

Flash the raspberry pi image with the [imager](https://www.raspberrypi.com/software/). For optimal performance use the *Raspberry Pi OS Lite* version.  
Make sure to enable ssh and add a user. This does not have to be the user pi but you have to remember the choosen username and password. Optional you may configure Wifi if needed but always on devices are better of with a wired ethernet connection.

When the Pi has booted up wait at least 10 or more minutes to let de postinstall do its work. Multiple restarts will occur.

After some time SSH login with the username and password you have choosen while creating the image. If you have trouble finding the IP addres of the Pi then look into your router which most likely has a table with DHCP addresses. 

Once logged in enter the following command:

```
curl -sSL https://gitlab.com/seventer/sip_setup/-/raw/main/sip_setup.sh | sudo bash
```

Then use TAB and SPACE to select the required options and select \<OK\> to continue.
![setup-1](img/sip_setup_ui1.png)

SIP can be installed in `/opt` (recommended) or users homedirectory.

![setup-2](img/sip_setup_ui2.png)

Choose the preferred location and press \<ENTER\> to continue.
![setup-3](img/sip_setup_ui3.png)

After the install reboot the Pi or exit first to review the logs.

***

## Support
>For generic questions please use the [SIP forum](https://nosack.com/sipforum/index.php) or use this Gitlab project when having issues with this script.