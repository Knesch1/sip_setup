
#! /bin/bash
###
# script: easy install SIP and requirements on a fresh Bullseye Pi image
# by: Gerard
# version: 0.2
# usage:  curl -sSL https://gitlab.com/seventer/sip_setup/-/raw/main/sip_experimental_setup.sh | sudo bash
###


if [[ $(id -u) -gt 0 ]]
then
    echo "run this command as root or sudo otherwise this script might fail"
    exit
fi

current_user=${SUDO_USER:-$USER}

do_upd_sys=false
do_i2c=false
do_mqtt=false
do_user_grp=false
do_log2ram=false
do_haproxy=false
install_location="/opt"
sip_port=8080


function gen_cert() {
  CPATH="/tmp/cert"
  SERVER_KEY="$CPATH/server.key"
  SERVER_CSR="$CPATH/server.csr"
  SERVER_CRT="$CPATH/server.crt"
  EXTFILE="$CPATH/cert_ext.cnf"
  OPENSSL_CMD="/usr/bin/openssl"
  
  mkdir -p $CPATH

  cat << EOF > $EXTFILE
[req]
default_bit = 4096
distinguished_name = req_distinguished_name
prompt = no

[req_distinguished_name]
countryName             = US
stateOrProvinceName     = SIP
localityName            = SIP
organizationName        = SIP
commonName              = $HOSTNAME
EOF

  # generating server key
  echo "Generating private key"
  $OPENSSL_CMD genrsa -out $SERVER_KEY  4096

  if [ $? -ne 0 ] ; then
    echo "ERROR: Failed to generate $SERVER_KEY"
    exit 1
  fi

  # Generating Certificate Signing Request using config file
  echo "Generating Certificate Signing Request"
  $OPENSSL_CMD req -new -key $SERVER_KEY -out $SERVER_CSR -config $EXTFILE

  if [ $? -ne 0 ] ; then
    echo "ERROR: Failed to generate $SERVER_CSR"
    exit 1
  fi

  echo "Generating self signed certificate"
  $OPENSSL_CMD x509 -req -days 3650 -in $SERVER_CSR -signkey $SERVER_KEY -out $SERVER_CRT

  if [ $? -ne 0 ] ; then
    echo "ERROR: Failed to generate self-signed certificate file $SERVER_CRT"
  fi

  cat $SERVER_KEY $SERVER_CRT >> /etc/ssl/sip_server.pem
  return 0
}




#
# show menu
#
CHOICES=$(whiptail --title " SIP setup " --separate-output --checklist  "Choose install options" 12 45 6 \
 "1" "Update system (recommended)" ON \
 "2" "Enable i2c" ON \
 "3" "Install MQTT broker" OFF \
 "4" "run SIP as user ${current_user}" ON \
 "5" "Install log2ram" ON  \
 "6" "Install haproxy" ON 3>&1 1>&2 2>&3) 

if [ -z "$CHOICES" ]; then
  echo "No option was selected or cancelled. Stopping script."
  exit 1
else
  for CHOICE in $CHOICES; do
    case "$CHOICE" in
    "1")
      do_upd_sys=true
      ;;
    "2")
      do_i2c=true
      ;;
    "3")
      do_mqtt=true
      ;;
    "4")
      do_user_grp=true
      ;;
    "5")
      do_log2ram=true
      ;;
    "6")
      do_haproxy=true
      ;;
    *)
      echo "Unsupported item $CHOICE!" >&2
      exit 1
      ;;
    esac
  done
fi

if (whiptail --title "Location" --yesno "Install SIP in /opt or the $current_user homedir?" --no-button "homedir" --yes-button "opt" 8 45); then
    echo "installing in /opt"
    mkdir -p /opt
    install_location="/opt"
else
    echo "installing in homedir"
    install_location="/home/$current_user"
fi


if [ "$do_upd_sys" = true ]; then
  echo ===== Updating system =====
  apt update && apt upgrade -y
fi

echo ===== Installing git =====
apt install git -y

if [ "$do_i2c" = true ]; then
  echo ===== Installing  i2c requirements =====
  apt install -y python3-smbus i2c-tools -y
  echo ===== Enabling  i2c interface =====
  raspi-config nonint do_i2c 0
fi

if [ "$do_mqtt" = true ]; then
  echo ===== Installing  mqtt broker and paho client =====
  apt install mosquitto -y
  apt install python3-paho-mqtt -y
fi

if [ "$do_user_grp" = true ]; then
  echo ===== adding user ${current_user} to hardware groups  =====
  usermod -aG gpio ${current_user}
  usermod -aG i2c ${current_user}
  usermod -aG dialout ${current_user}
  # allow user to start/stop sip
  cat << EOF > /etc/sudoers.d/010_sip_user
${current_user} ALL= NOPASSWD: /bin/systemctl start sip.service
${current_user} ALL= NOPASSWD: /bin/systemctl restart sip.service
${current_user} ALL= NOPASSWD: /bin/systemctl stop sip.service
EOF

fi


echo ===== Installing SIP =====
cd $install_location
git clone https://github.com/Dan-in-CA/SIP


echo ===== Creating and installing SystemD service =====
cat << EOF > /tmp/sip.service
#Service for SIP running on a SystemD service
#
[Unit]
Description=SIP for Python3
After=syslog.target network.target
[Service]
ExecStart=/usr/bin/python3 -u ${install_location}/SIP/sip.py
Restart=on-abort
WorkingDirectory=${install_location}/SIP/
EOF

if [ "$do_user_grp" = true ]; then
   cat << EOF >> /tmp/sip.service
User=${current_user}
Group=${current_user}
EOF
fi

cat << EOF >> /tmp/sip.service
SyslogIdentifier=sip
[Install]
WantedBy=multi-user.target
EOF

cp /tmp/sip.service /etc/systemd/system/
systemctl enable sip.service

echo ===== Starting SIP once to create config files =====
systemctl start sip.service
{
    for ((i = 0 ; i <= 100 ; i+=5)); do
        sleep 0.5
        echo $i
    done
} | whiptail --gauge "Wait while we start and stop SIP....." 6 50 0
systemctl stop sip.service



if [ "$do_haproxy" = true ]; then
  echo ===== Setting SIP http port to $sip_port =====
  sed -i "s/\"htp\": 80,/ \"htp\": $sip_port,/" ${install_location}/SIP/data/sd.json
  echo ===== Installing  haproxy =====
  if [ ! -f "/etc/haproxy/haproxy.cfg" ];
  then
    apt install haproxy -y
  else
    mv -f /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy_sip_saved.cfg
    apt-get -o Dpkg::Options::="--force-confmiss" install --reinstall haproxy
  fi
  

  gen_cert

cat << EOF > /tmp/haproxy_extra.cfg
  frontend public
        bind :::80 v4v6
        bind :::443 v4v6 ssl crt /etc/ssl/sip_server.pem
        option forwardfor except 127.0.0.1
        # uncomment for tls only
        #  redirect scheme https code 301 if !{ ssl_fc }
        default_backend sip

  backend sip
        acl needs_scheme req.hdr_cnt(X-Scheme) eq 0

        http-request replace-path ^([^\ :]*)\ /(.*) \1\ /\2
        http-request set-header X-Forwarded-Proto https if { ssl_fc }
        http-request set-header X-Forwarded-Proto http if !{ ssl_fc }
        option forwardfor
        server sip1 127.0.0.1:8080
EOF

  cat /tmp/haproxy_extra.cfg >> /etc/haproxy/haproxy.cfg
  systemctl restart haproxy

fi



if [ "$do_log2ram" = true ]; then
  echo ===== Installing log2ram =====
  cd /home/${current_user}
  wget https://github.com/azlux/log2ram/archive/master.tar.gz -O log2ram.tar.gz
  tar xf log2ram.tar.gz
  cd /home/${current_user}/log2ram-master
  ./install.sh
  echo ===== Increasing logsize to 100M =====
  sed -i "s/40M/100M/g" /etc/log2ram.conf
  cd ~
fi

echo ===== Done installing SIP and requirements. Please check the output above and reboot the Pi =====
systemctl start sip.service

if (whiptail --title "Setup finished" --yesno "Done installing SIP and requirements.\n\nReboot now or choose Exit to review the output of the setup " --no-button "Exit" --yes-button "Reboot" 10 65); then
    echo "Restarting....."
    reboot
else
    exit 1
fi


